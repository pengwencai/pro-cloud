/*
Navicat MySQL Data Transfer

Source Server         : 119.23.8.73
Source Server Version : 50726
Source Host           : 119.23.8.73:3306
Source Database       : pro_cloud

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-07-30 19:57:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gen_data_source
-- ----------------------------
DROP TABLE IF EXISTS `gen_data_source`;
CREATE TABLE `gen_data_source` (
  `id` bigint(20) unsigned NOT NULL COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `url` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='数据源表';

-- ----------------------------
-- Records of gen_data_source
-- ----------------------------
INSERT INTO `gen_data_source` VALUES ('2', 'database2', 'jdbc:mysql://119.23.8.73:3306/pro_cloud?autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&serverTimezone=UTC', 'aiiod', '12345667222', '1', '2019-06-13 23:24:03', '1', '2019-06-19 23:24:09', '', '0');

-- ----------------------------
-- Table structure for gen_scheme
-- ----------------------------
DROP TABLE IF EXISTS `gen_scheme`;
CREATE TABLE `gen_scheme` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `source_Id` bigint(20) NOT NULL COMMENT '数据源id',
  `author` varchar(100) NOT NULL COMMENT '作者名',
  `table_name` varchar(200) DEFAULT NULL COMMENT '表名',
  `category` char(1) DEFAULT NULL COMMENT '分类',
  `package_name` varchar(500) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `class_name` varchar(500) DEFAULT NULL COMMENT '生成功能名,也为实体名',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生成代码表';

-- ----------------------------
-- Records of gen_scheme
-- ----------------------------
INSERT INTO `gen_scheme` VALUES ('1', '0', 'Aijm', 'gen_scheme', '2', 'com.cloud', 'generator', null, '1', '2020-05-13 16:05:04', '1', '2020-05-16 22:25:56', '', '0');
INSERT INTO `gen_scheme` VALUES ('2', '0', 'Aijm', 'sys_user', '1', 'com.cloud', 'admin', null, '1', '2020-05-13 16:05:04', '1', '2020-05-16 16:04:57', '', '0');

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(256) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('1285495805901410304', 'admin,oss,websock', '$2a$10$NOzjM62p3pfbvgqXwipwsOg1ciczxxf2bTPcPo0dzuX8WjUJdM8HC', 'app', 'password,refresh_token', null, null, '10000', '100000', null, null, '11');
INSERT INTO `oauth_client_details` VALUES ('client', 'admin,oss,websock', '$2a$10$PXm/Y1blhC5BPUHL01syP.y0/llhmQdfGEszUUqSn5qNdKPP8.6Xm', 'app', 'password,refresh_token', null, '', '100000', '199000', '', null, '0');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型字典/树 0 列表 1树形',
  `type_code` varchar(100) DEFAULT NULL COMMENT '类型编码',
  `system` tinyint(1) DEFAULT '0' COMMENT '是否是系统级别数据 1 系统 0 非系统',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `type_code_tenant` (`type_code`,`tenant_id`) USING BTREE COMMENT 'typeCode和租户组合唯一'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '租户资源', '0', 'list_tentant_resource', '1', '1', '2020-07-11 22:49:36', '1', '2020-07-11 22:49:49', '租户拥有资源', '0', '0');
INSERT INTO `sys_dict` VALUES ('2', '用户类型', '0', 'list_user_type', '1', '1', '2020-07-11 22:59:10', '1', '2020-07-11 22:59:10', '用户类型', '0', '0');

-- ----------------------------
-- Table structure for sys_dict_list
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_list`;
CREATE TABLE `sys_dict_list` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `value` varchar(100) DEFAULT NULL COMMENT '值',
  `label` varchar(100) DEFAULT NULL COMMENT '标签',
  `type_code` varchar(100) DEFAULT NULL COMMENT '编码',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序（升序）',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典项list';

-- ----------------------------
-- Records of sys_dict_list
-- ----------------------------
INSERT INTO `sys_dict_list` VALUES ('1', 'admin中心', 'admin', '', 'list_tentant_resource', '10', '1', '2020-07-11 22:50:30', '1', '2020-07-30 19:51:55', '', '0', '0');
INSERT INTO `sys_dict_list` VALUES ('2', '阿里云oss存储', 'oss', '', 'list_tentant_resource', '20', '1', '2020-07-11 22:51:02', '1', '2020-07-11 22:51:02', '', '0', '0');
INSERT INTO `sys_dict_list` VALUES ('3', '短信/邮件', 'smsEmail', '', 'list_tentant_resource', '30', '1', '2020-07-11 22:53:35', '1', '2020-07-11 22:53:35', '', '0', '0');
INSERT INTO `sys_dict_list` VALUES ('4', '租户管理员', '0', '', 'list_user_type', '10', '1', '2020-07-11 22:59:29', '1', '2020-07-11 22:59:29', '', '0', '0');
INSERT INTO `sys_dict_list` VALUES ('5', '个人', '1', '', 'list_user_type', '20', '1', '2020-07-11 23:01:24', '1', '2020-07-11 23:01:24', '', '0', '0');

-- ----------------------------
-- Table structure for sys_dict_tree
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_tree`;
CREATE TABLE `sys_dict_tree` (
  `id` bigint(20) unsigned NOT NULL COMMENT '编号',
  `parent_id` bigint(20) unsigned NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(60) NOT NULL COMMENT '所有父级编号',
  `type_code` varchar(100) DEFAULT NULL COMMENT '字典类型code',
  `name` varchar(100) DEFAULT NULL COMMENT '标签',
  `value` varchar(100) DEFAULT NULL COMMENT '编码；一般唯一',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序（升序）',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典项tree';

-- ----------------------------
-- Records of sys_dict_tree
-- ----------------------------

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `belong_name` varchar(100) NOT NULL DEFAULT '' COMMENT '归属应用',
  `belong_type` varchar(100) NOT NULL DEFAULT '' COMMENT '归属应用类别',
  `belong_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '归属应用状态；0：开启 ',
  `pre_path` varchar(100) DEFAULT NULL COMMENT '文件地址前缀',
  `file_name` varchar(100) NOT NULL DEFAULT '' COMMENT '文件名称（）目录名称',
  `file_url` varchar(300) NOT NULL DEFAULT '' COMMENT '文件外网访问路径',
  `file_path` varchar(100) NOT NULL DEFAULT '' COMMENT '文件存储路径',
  `type` char(1) DEFAULT NULL COMMENT '文件类型(视频类型:0;音频类型:1; zip类型:2; doc类型:3; 图片类型:4; 其他类型:9;)',
  `file_size` bigint(50) DEFAULT '0' COMMENT '文件大小',
  `file_suffix` varchar(10) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `props` varchar(20) DEFAULT '' COMMENT '文件属性',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件统一管理(目录也是文件一种)';

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES ('1177472123753598976', 'default', 'default', '4', 'default', '1.csv', '', 'default/default/default/20190927/142729/1.csv', '3', '0', 'csv', '', '1', '2019-09-27 14:36:45', '1', '2019-09-27 14:36:45', '', '0', '0');
INSERT INTO `sys_file` VALUES ('1178500048879751168', 'default', 'default', '8', 'default', '1.csv', '', 'default/default/default/20190930/103959/1.csv', '3', '51', 'csv', '', '1', '2019-09-30 10:41:22', '1', '2019-09-30 10:41:22', '', '0', '0');
INSERT INTO `sys_file` VALUES ('1259469794223198208', 'cloud', 'admin', '0', 'profile/photo', '22.png', '', 'cloud/admin/profile/photo/200510/210612/22.png', '4', '47210', 'png', '1139*621', '1', '2020-05-10 21:06:13', '1', '2020-05-10 21:06:13', '', '0', '0');
INSERT INTO `sys_file` VALUES ('1259477046225145856', 'cloud', 'admin', '0', 'profile/photo', '2689526-874dc44e81a0bcd1.png', '', 'cloud/admin/profile/photo/20200510/213500/2689526-874dc44e81a0bcd1.png', '4', '59849', 'png', '772*183', '1', '2020-05-10 21:35:02', '1', '2020-05-10 21:35:02', '', '0', '0');
INSERT INTO `sys_file` VALUES ('1259480871262621696', 'cloud', 'admin', '0', 'profile/photo', 'P}KB~WLGPKSPDKIVUXCLGH3.png', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 'cloud/admin/profile/photo/20200510/215012/P}KB~WLGPKSPDKIVUXCLGH3.png', '4', '237689', 'png', '732*510', '1', '2020-05-10 21:50:14', '1', '2020-05-10 21:50:14', '', '0', '0');
INSERT INTO `sys_file` VALUES ('1259481106646962176', 'cloud', 'admin', '0', 'profile/photo', 'P}KB~WLGPKSPDKIVUXCLGH3.png', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 'cloud/admin/profile/photo/20200510/215108/P}KB~WLGPKSPDKIVUXCLGH3.png', '4', '237689', 'png', '732*510', '1', '2020-05-10 21:51:11', '1', '2020-05-10 21:51:11', '', '0', '0');
INSERT INTO `sys_file` VALUES ('1259481310766960640', 'cloud', 'admin', '0', 'profile/photo', 'P}KB~WLGPKSPDKIVUXCLGH3.png', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 'cloud/admin/profile/photo/20200510/215157/P}KB~WLGPKSPDKIVUXCLGH3.png', '4', '237689', 'png', '732*510', '1', '2020-05-10 21:51:59', '1', '2020-05-10 21:51:59', '', '0', '0');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) unsigned NOT NULL COMMENT '编号',
  `parent_id` bigint(20) unsigned NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(60) NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` decimal(10,0) NOT NULL COMMENT '排序',
  `type` tinyint(1) unsigned DEFAULT NULL COMMENT '菜单类型0 目录 1 组建 2.外部链接 3按钮',
  `target` varchar(100) DEFAULT '' COMMENT '目标',
  `icon` varchar(100) DEFAULT '' COMMENT '图标',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否在菜单中显示(1 显示 0 隐藏)',
  `permission` varchar(200) DEFAULT NULL COMMENT '权限标识',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`),
  KEY `sys_menu_parent_id` (`parent_id`),
  KEY `sys_menu_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '0,', '系统设置', '900', '0', '/admin', 'fa fa-gear', '1', '', '1', '2020-05-01 08:00:00', '1', '2020-05-24 21:24:51', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '0,1,', '菜单管理', '100', '1', '/admin/sysmenu', 'fa fa-tachometer', '1', '', '1', '2020-05-01 08:00:00', '1', '2020-05-24 21:29:53', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('3', '2', '0,1,2,', '添加菜单', '30', '3', '', '', '0', 'admin_sysmenu_add', '1', '2020-05-06 22:42:36', '1', '2020-05-06 22:48:19', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('4', '2', '0,1,2,', '修改菜单', '30', '3', '', '', '0', 'admin_sysmenu_edit', '1', '2020-05-06 22:43:11', '1', '2020-05-06 22:48:30', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('5', '2', '0,1,2,', '删除菜单', '30', '3', '', '', '0', 'admin_sysmenu_del', '1', '2020-05-06 22:43:35', '1', '2020-05-06 22:47:57', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('6', '2', '0,1,2,', '查询菜单', '30', '3', '', '', '0', 'admin_sysmenu_view', '1', '2020-05-06 22:45:17', '1', '2020-05-06 22:48:08', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('7', '1', '0,1,', '用户管理', '30', '1', '/admin/sysuser', 'fa fa-group', '1', '', '1', '2020-05-06 22:46:59', '1', '2020-05-24 21:28:04', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('8', '7', '0,1,7,', '添加用户', '30', '3', '', '', '0', 'admin_sysuser_add', '1', '2020-05-06 22:47:37', '1', '2020-05-26 23:41:00', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('9', '7', '0,1,7,', '修改用户', '30', '3', '', '', '0', 'admin_sysuser_edit', '1', '2020-05-06 22:49:33', '1', '2020-05-06 22:49:33', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('10', '7', '0,1,7,', '删除用户', '30', '3', '', '', '0', 'admin_sysuser_del', '1', '2020-05-06 22:50:00', '1', '2020-05-06 22:50:17', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('11', '7', '0,1,7,', '查询用户', '30', '3', '', '', '0', 'admin_sysuser_view', '1', '2020-05-06 22:50:40', '1', '2020-07-08 22:41:43', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('12', '1', '0,1,', '字典管理', '30', '1', '/admin/sysdict', 'fa fa-codepen', '1', '', '1', '2020-05-10 00:08:26', '1', '2020-07-11 22:07:24', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('13', '1', '0,1,', '部门管理', '30', '1', '/admin/sysoffice', 'fa fa-user', '1', '', '1', '2020-05-10 16:03:19', '1', '2020-07-11 22:07:54', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('14', '1', '0,1,', '角色管理', '30', '1', '/admin/sysrole', 'fa fa-user-circle', '1', '', '1', '2020-05-10 16:05:11', '1', '2020-07-11 22:07:40', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('15', '1', '0,1,', '代码生成', '30', '1', '/generator', 'fa fa-fa', '1', '', '1', '2020-05-16 22:01:35', '1', '2020-07-11 22:07:07', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('16', '0', '0,', '监控管理', '30', '0', '', 'fa fa-user', '1', '', '1', '2020-05-21 22:48:34', '1', '2020-07-10 23:37:45', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('17', '1', '0,1,', '租户管理', '30', '1', '/admin/systenant', 'fa fa-cloud', '1', '', '1', '2020-05-25 13:51:42', '1', '2020-07-11 13:13:08', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('18', '16', '0,16,', '接口文档', '30', '1', '/swagger', 'fa fa-file-code-o', '1', '', '1', '2020-07-11 22:14:08', '1', '2020-07-11 22:14:08', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('19', '12', '0,1,12,', '添加DICT', '30', '3', '', '', '0', 'admin_sysdict_add', '1', '2020-05-06 22:47:37', '1', '2020-05-26 23:41:00', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('20', '12', '0,1,12,', '修改DICT', '30', '3', '', '', '0', 'admin_sysdict_edit', '1', '2020-05-06 22:49:33', '1', '2020-05-06 22:49:33', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('21', '12', '0,1,12,', '删除DICT', '30', '3', '', '', '0', 'admin_sysdict_del', '1', '2020-05-06 22:50:00', '1', '2020-05-06 22:50:17', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('22', '12', '0,1,12,', '查询DICT', '30', '3', '', '', '0', 'admin_sysdict_view', '1', '2020-05-06 22:50:40', '1', '2020-07-08 22:41:43', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('23', '12', '0,1,12,', '添加DICTList', '30', '3', '', '', '0', 'admin_sysdictlist_add', '1', '2020-05-06 22:47:37', '1', '2020-05-26 23:41:00', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('24', '12', '0,1,12,', '修改DICTList', '30', '3', '', '', '0', 'admin_sysdictlist_edit', '1', '2020-05-06 22:49:33', '1', '2020-05-06 22:49:33', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('25', '12', '0,1,12,', '删除DICTList', '30', '3', '', '', '0', 'admin_sysdictlist_del', '1', '2020-05-06 22:50:00', '1', '2020-05-06 22:50:17', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('26', '12', '0,1,12,', '查询DICTList', '30', '3', '', '', '0', 'admin_sysdictlist_view', '1', '2020-05-06 22:50:40', '1', '2020-07-08 22:41:43', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('27', '12', '0,1,12,', '添加DICTTree', '30', '3', '', '', '0', 'admin_sysdicttree_add', '1', '2020-05-06 22:47:37', '1', '2020-05-26 23:41:00', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('28', '12', '0,1,12,', '修改DICTTree', '30', '3', '', '', '0', 'admin_sysdicttree_edit', '1', '2020-05-06 22:49:33', '1', '2020-05-06 22:49:33', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('29', '12', '0,1,12,', '删除DICTTree', '30', '3', '', '', '0', 'admin_sysdicttree_del', '1', '2020-05-06 22:50:00', '1', '2020-05-06 22:50:17', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('30', '12', '0,1,12,', '查询DICTTree', '30', '3', '', '', '0', 'admin_sysdicttree_view', '1', '2020-05-06 22:50:40', '1', '2020-07-08 22:41:43', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('31', '14', '0,1,14,', '添加角色', '30', '3', '', '', '0', 'admin_sysrole_add', '1', '2020-05-06 22:47:37', '1', '2020-05-26 23:41:00', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('32', '14', '0,1,14,', '修改角色', '30', '3', '', '', '0', 'admin_sysrole_edit', '1', '2020-05-06 22:49:33', '1', '2020-05-06 22:49:33', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('33', '14', '0,1,14,', '删除角色', '30', '3', '', '', '0', 'admin_sysrole_del', '1', '2020-05-06 22:50:00', '1', '2020-05-06 22:50:17', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('34', '14', '0,1,14,', '查询角色', '30', '3', '', '', '0', 'admin_sysrole_view', '1', '2020-05-06 22:50:40', '1', '2020-07-08 22:41:43', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('35', '13', '0,1,13,', '添加部门', '30', '3', '', '', '0', 'admin_sysoffice_add', '1', '2020-05-06 22:47:37', '1', '2020-05-26 23:41:00', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('36', '13', '0,1,13,', '修改部门', '30', '3', '', '', '0', 'admin_sysoffice_edit', '1', '2020-05-06 22:49:33', '1', '2020-05-06 22:49:33', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('37', '13', '0,1,13,', '删除部门', '30', '3', '', '', '0', 'admin_sysoffice_del', '1', '2020-05-06 22:50:00', '1', '2020-05-06 22:50:17', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('38', '13', '0,1,13,', '查询部门', '30', '3', '', '', '0', 'admin_sysoffice_view', '1', '2020-05-06 22:50:40', '1', '2020-07-08 22:41:43', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('39', '17', '0,1,17,', '添加租户', '30', '3', '', '', '0', 'admin_systenant_add', '1', '2020-05-06 22:47:37', '1', '2020-05-26 23:41:00', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('40', '17', '0,1,17,', '修改租户', '30', '3', '', '', '0', 'admin_systenant_edit', '1', '2020-05-06 22:49:33', '1', '2020-05-06 22:49:33', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('41', '17', '0,1,17,', '删除租户', '30', '3', '', '', '0', 'admin_systenant_del', '1', '2020-05-06 22:50:00', '1', '2020-05-06 22:50:17', '', '0', '0');
INSERT INTO `sys_menu` VALUES ('42', '17', '0,1,17,', '查询租户', '30', '3', '', '', '0', 'admin_systenant_view', '1', '2020-05-06 22:50:40', '1', '2020-07-08 22:41:43', '', '0', '0');

-- ----------------------------
-- Table structure for sys_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_office`;
CREATE TABLE `sys_office` (
  `id` bigint(20) unsigned NOT NULL COMMENT '编号',
  `parent_id` bigint(20) unsigned NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(60) NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` decimal(10,0) NOT NULL COMMENT '排序',
  `address` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `phone` varchar(200) DEFAULT NULL COMMENT '电话',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `primary_person` varchar(64) DEFAULT NULL COMMENT '主负责人',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态0 启用 1 禁用',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`),
  KEY `sys_office_parent_id` (`parent_id`),
  KEY `sys_office_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构表';

-- ----------------------------
-- Records of sys_office
-- ----------------------------
INSERT INTO `sys_office` VALUES ('1', '0', '0,', '江西省总公司', '10', '江西省抚州市', '18210584444', '2929783435@qq.com', 'CEO', '0', '1', '2019-11-25 08:00:00', '1', '2020-05-24 15:39:56', '江西省', '0', '0');
INSERT INTO `sys_office` VALUES ('2', '1', '0,1,', '分公司A', '10', '江西省抚州市', '18210584444', '2929783435@qq.com', 'CEO', '0', '1', '2019-11-25 08:00:00', '1', '2019-11-25 22:36:51', '江西省分公司A', '0', '0');
INSERT INTO `sys_office` VALUES ('3', '1', '0,1,', '分公司B', '10', '江西省抚州市', '18210584444', '2929783435@qq.com', 'CEO', '0', '1', '2019-11-25 08:00:00', '1', '2019-11-25 22:36:51', '江西省分公司B', '0', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) unsigned NOT NULL COMMENT '编号',
  `name` varchar(100) NOT NULL COMMENT '角色名称',
  `enname` varchar(255) DEFAULT NULL COMMENT '英文名称',
  `role_type` tinyint(1) DEFAULT NULL COMMENT '角色类型',
  `data_scope` tinyint(1) DEFAULT NULL COMMENT '数据范围',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`),
  KEY `sys_role_del_flag` (`del_flag`),
  KEY `sys_role_enname` (`enname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '系统管理员', 'admin', '1', '1', '1', '2020-05-24 21:34:04', '1', '2020-07-11 12:55:25', '', '0', '0');
INSERT INTO `sys_role` VALUES ('2', '公司管理员', 'ceo', '1', '2', '1', '2020-05-24 21:34:04', '1', '2020-05-24 21:34:04', ' ', '0', '0');
INSERT INTO `sys_role` VALUES ('3', '部门管理员', 'dept', '1', '3', '1', '2020-05-24 21:34:04', '1', '2020-05-24 21:34:04', ' ', '0', '0');
INSERT INTO `sys_role` VALUES ('4', '普通用户', 'd', '1', '2', '1', '2020-05-24 21:34:04', '1', '2020-07-26 22:25:41', ' ', '0', '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色编号',
  `menu_id` bigint(20) unsigned NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色-菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('4', '1');
INSERT INTO `sys_role_menu` VALUES ('4', '2');
INSERT INTO `sys_role_menu` VALUES ('4', '6');
INSERT INTO `sys_role_menu` VALUES ('4', '7');
INSERT INTO `sys_role_menu` VALUES ('4', '11');
INSERT INTO `sys_role_menu` VALUES ('4', '12');
INSERT INTO `sys_role_menu` VALUES ('4', '13');
INSERT INTO `sys_role_menu` VALUES ('4', '14');
INSERT INTO `sys_role_menu` VALUES ('4', '15');
INSERT INTO `sys_role_menu` VALUES ('4', '16');
INSERT INTO `sys_role_menu` VALUES ('4', '17');
INSERT INTO `sys_role_menu` VALUES ('4', '18');
INSERT INTO `sys_role_menu` VALUES ('4', '22');
INSERT INTO `sys_role_menu` VALUES ('4', '26');
INSERT INTO `sys_role_menu` VALUES ('4', '30');
INSERT INTO `sys_role_menu` VALUES ('4', '34');
INSERT INTO `sys_role_menu` VALUES ('4', '38');
INSERT INTO `sys_role_menu` VALUES ('4', '42');

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant` (
  `id` bigint(20) unsigned NOT NULL COMMENT '编号',
  `type` tinyint(4) DEFAULT '0' COMMENT '类型0:租户1:对接方',
  `company_name` varchar(200) DEFAULT NULL COMMENT '租户名称(公司名称)',
  `login_name` varchar(100) NOT NULL COMMENT '租户管理登录用户名(为超级管理员)',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `user_name` varchar(100) NOT NULL COMMENT '姓名',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机',
  `qq_num` varchar(50) DEFAULT NULL COMMENT 'qq号',
  `wx_num` varchar(50) DEFAULT NULL COMMENT '微信号',
  `client_id` varchar(256) DEFAULT NULL COMMENT '客户端id',
  `beg_date` datetime NOT NULL COMMENT '开始时间',
  `end_date` datetime NOT NULL COMMENT '结束时间',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态0 启用 1 禁用',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='租户管理';

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES ('1', '0', '南昌有限公司', 'myadmin', '$2a$10$EkMn2cTP4fbB00z4TO.KmuwcqAmFQBd3P2qbZCnrrbOiRYcdFd9X2', 'aijm', null, null, '18210584253', null, null, 'client', '2020-07-08 00:00:00', '2020-07-18 23:59:59', '0', '1', '2020-05-26 00:17:24', '1', '2020-07-19 22:05:31', '', '0', '0');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) unsigned NOT NULL COMMENT '编号',
  `office_id` bigint(20) unsigned NOT NULL COMMENT '归属部门',
  `login_name` varchar(100) NOT NULL COMMENT '登录名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机',
  `user_type` tinyint(4) unsigned DEFAULT NULL COMMENT '用户类型0：租户管理员 1:普通',
  `photo` varchar(300) DEFAULT NULL COMMENT '用户头像',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '状态0 启用 1 禁用',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `create_by` bigint(20) unsigned NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` bigint(20) unsigned NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) NOT NULL DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '删除标记',
  `wx_openid` varchar(32) DEFAULT '' COMMENT '微信id',
  `qq_openid` varchar(32) DEFAULT '' COMMENT 'qq id',
  `tenant_id` int(11) DEFAULT '0' COMMENT '租户id',
  PRIMARY KEY (`id`),
  KEY `sys_user_office_id` (`office_id`),
  KEY `sys_user_login_name` (`login_name`),
  KEY `sys_user_update_date` (`update_date`),
  KEY `sys_user_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '2', 'myadmin', '$2a$10$k9tBdw0GHbLtToX2Apheq.bR/D2WYWb5Rq0oPJt1Hn2axpsA.CI9O', '系统管理员', '2929783435@qq.com', '18210584253', '18210584253', '0', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '0', '2020-06-22 23:47:46', '127.0.0.1', '1', '2013-05-27 08:00:00', '1', '2020-07-17 18:16:50', ' ', '0', '', '', '0');
INSERT INTO `sys_user` VALUES ('2', '2', 'admin', '$2a$10$k9tBdw0GHbLtToX2Apheq.bR/D2WYWb5Rq0oPJt1Hn2axpsA.CI9O', '系统管理员', '292978343x@qq.com', '', '', '1', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '0', '2020-06-22 23:47:46', '127.0.0.1', '1', '2013-05-27 08:00:00', '1', '2020-07-26 22:24:39', ' ', '0', '', '', '0');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户编号',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户-角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '4');
