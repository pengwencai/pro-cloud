/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.oss.entity;

import lombok.Data;

import java.io.Serializable;

/**
 *  回调传递参数
 *  belongName/belongType/prePath/yyyyMMdd/HHmmss/上传文件名
 * @author Aijm
 * @since 2019/9/11
 */
@Data
public class CallBack implements Serializable {
    private static final long serialVersionUID=1L;


    /**
     * 租户id
     */
    private Integer tenantId;
    /**
     * 上传文件的用户id
     */
    private Long userId;
    /**
     * 文件外网访问路径
     */
    private String fileUrl;
    /**
     * oss文件的路径存储地址
     */
    private String filePath;

    /**
     * 文件地址前缀
     */
    private String prePath;

    /**
     * 归属应用
     */
    private String belongName;

    /**
     * 归属应用类别
     */
    private String belongType;

    /**
     * 文件的大小
     */
    private long size;
    private String mimeType;

    private String width;
    private String height;


}