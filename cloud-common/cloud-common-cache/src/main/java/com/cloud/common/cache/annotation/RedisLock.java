/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.cache.annotation;

import com.cloud.common.cache.constants.RedisKeys;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;


/**
 * @Author Aijm
 * @Description   基于redis的分布式锁注解
 * @Date 2019/8/19
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface RedisLock {

    /**
     * 默认锁的前缀
     * @return
     */
    String preKey() default RedisKeys.PRE_DEFAULT_LOCK;
    /**
     * 分布式锁的键
     */
    String key() default RedisKeys.DEFAULT_LOCK;

    /**
     * 锁释放时间，默认五秒
     */
    long timeOut() default RedisKeys.DEFAULT_TIMEOUT;

    /**
     * 尝试在指定时间内加锁(可以理解为阻塞时间)
     * @return
     */
    long timeBlock() default RedisKeys.DEFAULT_TIMEBLOCK;

    /**
     * 时间格式，默认：毫秒
     */
    TimeUnit timeUnit() default TimeUnit.MILLISECONDS;
}