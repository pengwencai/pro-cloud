/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.util.var;

import com.cloud.common.util.client.CloudServiceList;
import lombok.experimental.UtilityClass;

/**
 * @Author Aijm
 * @Description 静态常用数据
 * @Date 2019/7/23
 */
@UtilityClass
public class StaticVar {


    /**
     * 获取token的地址
     */
    public static final String LOGIN_URL = "http://"+ CloudServiceList.CLOUD_AUTH+ "/oauth/token";

    /**
     * 传递的 访问租户id
     */
    public static final String HEAD_USER_TENANT = "user-tenant";

    /**
     * tenantId
     */
    public static final String TENANT_ID = "tenantId";

    /**
     *  当前用户id
     */
    public static final String USERID_ID = "userId";

    /**
     * 用户类型
     */
    public static final String USER_TYPE = "userType";

    /**
     * 获取到用户类型
     */
    public static final String USER_NAME = "userName";

    /**
     * 默认的创建用户登录密码不设置时
     */
    public static final String DEFAULT_USER_PASSWORD = "123456";

    /**
     * 默认的租户值
     */
    public static final Integer TENANT_ID_DEFAULT = 0;

    /**
     * 租户类型
     */
    public static final Integer TENANT_TYPE_TENANT = 0;
    /**
     * 租户类型 第三方
     */
    public static final Integer TENANT_TYPE_THREE = 1;


    /**
     * 租户超级管理员用户类型
     */
    public static final Integer DEFAULT_USERTYPE_ADMIN  = 0;


    /**
     * 灰度发布头信息
     */
    public static final String GRAY_VERSION = "version";

}