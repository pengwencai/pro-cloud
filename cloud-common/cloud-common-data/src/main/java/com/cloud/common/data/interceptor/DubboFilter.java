/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.interceptor;


import com.cloud.common.data.util.SystemUtil;
import com.cloud.common.util.var.StaticVar;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 处理租户是否停用
 * @author Aijm
 * @since 2020/5/24
 */
@Slf4j
@Component
@AllArgsConstructor
public class DubboFilter extends OncePerRequestFilter {


    /**
     * 将租户信息添加到
     * @param request
     * @param response
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

        RpcContext.getContext().setAttachment(StaticVar.TENANT_ID, SystemUtil.getCurrentTenant().toString());
        // 获取用户id
        Long userId = SystemUtil.getUserId();
        Integer userType = SystemUtil.getUserType();
        RpcContext.getContext().setAttachment(StaticVar.USERID_ID, userId != null? userId.toString(): "");
        RpcContext.getContext().setAttachment(StaticVar.USER_TYPE, userType != null? userType.toString(): "");
        RpcContext.getContext().setAttachment(StaticVar.USER_NAME, SystemUtil.getUserName());
        chain.doFilter(request, response);
    }



}