/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.user;


/**
 * 用户信息接口 主要提供给ObjUtil
 * @author Aijm
 * @since 2019/8/25
 */
public interface SystemService {

    /**
     * 获取到登录用户的id
     * @return
     */
    Long getUserId();


    /**
     * 获取到用户的租户id
     * @return
     */
    Integer getUserTenantId();


    /**
     * 获取到用户类型
     * @return
     */
    Integer getUserType();

    /**
     * 获取到用户名称
     * @return
     */
    String getUserName();

}