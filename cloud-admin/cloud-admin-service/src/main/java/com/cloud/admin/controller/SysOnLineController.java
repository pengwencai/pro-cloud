/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.controller;


import com.cloud.common.data.base.BaseController;
import com.cloud.common.util.base.Result;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.Map;

/**
 * @Author Aijm
 * @Description 在线用户的管理
 * @Date 2019/12/27
 */
@RestController
@RequestMapping("/online")
public class SysOnLineController extends BaseController {


    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 查询token
     *
     * @param params 分页参数
     * @return
     */
    @PreAuthorize("@pms.hasPermission('admin_online_view')")
    @PostMapping("/page")
    public Result tokenList(@RequestBody Map<String, Object> params) {
        //根据分页参数获取对应数据
        // todo
        return Result.success("");
    }

}