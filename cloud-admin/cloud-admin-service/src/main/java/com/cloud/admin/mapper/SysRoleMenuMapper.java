/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.admin.beans.dto.RoleDTO;
import com.cloud.admin.beans.po.SysRoleMenu;

/**
 * 角色-菜单
 *
 * @author Aijm
 * @date 2019-08-25 21:12:49
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    /**
     * 批量插入 角色和菜单的关联关系
     * @param roleDTO
     * @return
     */
    int insertRoleMenu(RoleDTO roleDTO);


    /**
     *  删除角色菜单管理关系
     * @param roleId
     * @return
     */
    int deleteRoleDTO(Long roleId);
}